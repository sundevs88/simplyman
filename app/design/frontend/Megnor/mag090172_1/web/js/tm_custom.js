define(['jquery',], function($) {
    "use strict";
    
    
	require(['jquery', 'owlcarousel', 'fancybox', 'jstree', 'flexslider'], function($) {   

	    jQuery(window).load(function() {	
	  		jQuery("#spinner").fadeOut("slow");
		});

		jQuery(document).ready(function() {

		 	jQuery(".lightbox").fancybox({
				'frameWidth' : 890,
		    	'frameHeight' : 630,
				openEffect  : 'fade',
				closeEffect : 'fade',
				helpers: {
		        	title: null
	    		}
			});

			$("body").append("<a class='top_button' title='Back To Top' href=''>TOP</a>");

			$(window).scroll(function () {
				if ($(this).scrollTop() > 70) {
					$('.top_button').fadeIn();
				} else {
					$('.top_button').fadeOut();
				}
			});

			// scroll body to 0px on click
			$('.top_button').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
					return false;
			});

	          	
			jQuery("#blog-carousel").owlCarousel({
				nav: true,
				loop: false,
				items: 3,
				responsive: {
				0: {
				items: 1
				},
					480: {
				items: 2
				},
					768: {
				items: 2
				},
					1024: {
				items: 3
				}
				},
				navText: [
				  "<i class='icon-chevron-left icon-white'><</i>",
				  "<i class='icon-chevron-right icon-white'>></i>"
				 ]
	        });
					
			jQuery("#brand-carousel").owlCarousel({
				nav: true,
				loop: false,
				items: 5,
				responsive: {
				0: {
				items: 1
				},
					480: {
				items: 3
				},
					768: {
				items: 4
				},
					1024: {
				items: 5
				}
				},
				navText: [
				  "<i class='icon-chevron-left icon-white'><</i>",
				  "<i class='icon-chevron-right icon-white'>></i>"
				 ]
			});
			
			jQuery('#parallex').owlCarousel({nav: true,
				loop: false,
				items: 1
			});

	        jQuery('.products-carousel .owl-carousel').owlCarousel({
				items: 4,
				responsive: {
					0: {
					    items: 1
					},
							480: {
					    items: 2
					},
							641: {
					    items: 3
					},
							768: {
					    items: 3
					},
							1024: {
					    items: 4
					}
				    },
				navText: [
				  "<i class='icon-chevron-left icon-white'><</i>",
				  "<i class='icon-chevron-right icon-white'>></i>"
				 ]
	        });

	        jQuery('#category_blog-carousel').owlCarousel({
				items: 6,
				nav: true,
				loop: false,
				responsive: {
				0: {
				    items: 1
				},
						480: {
				    items: 2
				},
						768: {
				    items: 3
				},
						1024: {
				    items: 4
				},
						 1600 : {
				    items: 5
				}
				}
	        });

			jQuery('.flexslider').flexslider({
			    animation: "slide"
			});

			
			jQuery("#category-treeview").treeview({
				animated: "slow",
				collapsed: true,
				unique: true
			});
			
			function productListAutoSet(){
				//var $owl = jQuery('.product_tabs .widget-product-carousel');
				//$owl.trigger('destroy.owl.carousel');
				//$owl.html($owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');	
				jQuery('.widget-product-carousel').owlCarousel({			
					items: 4,
					responsive: {
					0: {
					    items: 1
					},
							480: {
					    items: 2
					},
							641: {
					    items: 3
					},
							768: {
					    items: 3
					},
							1024: {
					    items: 4
					}
					},
					navText: [
					"<i class='icon-chevron-left icon-white'><</i>",
					"<i class='icon-chevron-right icon-white'>></i>"
					]
				});	
			}
			jQuery(document).ready(function(){productListAutoSet();});
			jQuery(window).resize(function() {productListAutoSet();});

		
			jQuery(".tab_product:not(:first)").hide();
			 
			//to fix u know who
			jQuery(".tab_product:first").show();
			 
			//when we click one of the tabs
			jQuery(".tabbernav_product  li  a").click(function(){
		
				//get the ID of the element we need to show
				stringref = jQuery(this).attr("href").split('#')[1];
				
				//hide the tabs that doesn't match the ID
				jQuery('.tab_product:not(#'+stringref+')').hide();
				 //fix
				if (jQuery.browser.msie && jQuery.browser.version.substr(0,3) == "6.0") {
				 	jQuery('.tab_product#' + stringref).show();
				}
				else{
					//display our tab fading it in
					jQuery('.tab_product#' + stringref).fadeIn();
				}

				//alert("productListAutoSet");
				jQuery(".tabbernav_product a").removeClass("selected");
				jQuery(this).addClass("selected");
				
				var $owl = jQuery('#'+stringref+' .widget-product-carousel');
				$owl.trigger('destroy.owl.carousel');
				$owl.html($owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');	
				productListAutoSet();

				//stay with me
				return false;
			});


			jQuery("#testimonial-carousel").owlCarousel({
				nav: false,
				loop: false,
				items: 1,
				responsive: {
				0: {
				items: 1
				},
					480: {
				items: 1
				},
					768: {
				items: 1
				},
					1024: {
				items: 1
				}
				},
				navText: [
				  "<i class='icon-chevron-left icon-white'><</i>",
				  "<i class='icon-chevron-right icon-white'>></i>"
				 ]
	        });


	    }); //jQuery(document).ready(function(){ END(); });

		$("#mainmenu .menu-dropdown-icon ul").addClass("level0 submenu");
		$("#mainmenu .menu-dropdown-icon.hidden_menu li").addClass("level-top");
		$("#mainmenu .menu-dropdown-icon.hidden_menu ul").removeClass("level0 submenu");


	/***  Header link  ***/

	 jQuery(function($){
		if($(window).width()>1450){
			var max_elem = 8;
		}else if($(window).width()>1200){
			var max_elem = 8;
		}else if($(window).width()>1023){
			var max_elem = 8;
		}else if($(window).width()>641){
			var max_elem = 8;
		}else if($(window).width()>479){
			var max_elem = 5;
		}
		//else {var max_elem = 10;}
		var items = $('.static-link .static-link-inner > li');
		var surplus = items.slice(max_elem, (items.length));
		surplus.wrapAll('<li class="menu-dropdown-icon level0 hiden_menu"><ul class="dropdown-inner-list">');
		$('.hiden_menu').prepend('<a class="level-top">More</a>');
		});

		function mobileHeaderLink(){  
			jQuery(".tm_headerlinks_inner" ).addClass('toggle');	 
			jQuery(".tm_headerlinks_inner .headertoggle_img").click(function(){
				jQuery(this).parent().toggleClass('active').parent().find('.tm_headerlinks').slideToggle(0);
			});
		}
		jQuery(document).ready(function(){mobileHeaderLink();});

		function  checkoutEstimateTax(){  
        jQuery("#block-shipping .title").click(function(){
            jQuery(this).parent().toggleClass('active').parent().find('#block-shipping .content').slideToggle("fast");
            jQuery("#block-shipping .title").parent().find('#block-shipping .content').removeAttr('style');
        	});
    	}
	    jQuery(document).ready(function(){checkoutEstimateTax();});
	    jQuery(document).load(function(){checkoutEstimateTax();});


		function HeaderStaticLinksToggleMenu(){
			if (jQuery(window).width() < 480)
			{
				jQuery(".static-link").addClass('toggle');
				jQuery(" .static-link .mobile_togglemenu").click(function(){
					jQuery(this).parent().toggleClass('active').find('ul').toggle('slow');
				});
			}else{

				jQuery(".static-link").find('ul').removeAttr('style');
				jQuery(".static-link").removeClass('active');
				jQuery(".static-link").removeClass('toggle');
			}	
		}
		jQuery(document).ready(function(){HeaderStaticLinksToggleMenu();});
		//jQuery(window).resize(function(){HeaderStaticLinksToggleMenu();}); 


		function HeaderToggleMenu(){
			if (jQuery(window).width() > 979)
			{
				jQuery('.navigation.custommenu').prependTo(jQuery('.sidebar').first());
			}else{
				jQuery('.navigation.custommenu').appendTo(jQuery('#store-menu'));
				
			}	
		}
		jQuery(document).ready(function(){HeaderToggleMenu();});
		jQuery(window).resize(function(){HeaderToggleMenu();}); 

		function custommenuToggle(){
			if (jQuery(window).width() > 979)
			{
				if($('body').hasClass('page-layout-2columns-left') == true){
		  	 		jQuery('.custommenu .menu-title').addClass('active');
					jQuery('.custommenu .menu-title').click(function() {
						jQuery('.custommenu #mainmenu').slideToggle('slow');
						jQuery('.custommenu .menu-title').toggleClass('active');
					});
	  			}else{
					jQuery('.custommenu .menu-title').removeClass('active');
					jQuery('.custommenu .menu-title').click(function() {
						jQuery('.custommenu #mainmenu').slideToggle('slow');
						jQuery('.custommenu .menu-title').toggleClass('active');
					});
				}
			}else{
				jQuery('.custommenu .menu-title').click(function() {
						jQuery('.custommenu #mainmenu').slideToggle('slow');
						jQuery('.custommenu .menu-title').toggleClass('active');
					});
			}		
		}
		jQuery(document).ready(function(){custommenuToggle();});
		//jQuery(window).resize(function(){custommenuToggle();});



		function footerToggleMenu(){
			if (jQuery(window).width() < 980)
			{
				jQuery(".page-footer .footer-area .mobile_togglemenu").remove();
				jQuery(".page-footer .footer-area h6").append( "<a class='mobile_togglemenu'>&nbsp;</a>" );
				jQuery(".page-footer .footer-area h6").addClass('toggle');
				jQuery(".page-footer .footer-area .mobile_togglemenu").click(function(){
					jQuery(this).parent().toggleClass('active').parent().find('ul').toggle('slow');
				});
			}else{
				jQuery(".page-footer .footer-area h6").parent().find('ul').removeAttr('style');
				jQuery(".page-footer .footer-area  h6").removeClass('active');
				jQuery(".page-footer .footer-area  h6").removeClass('toggle');
				jQuery(".page-footer .mobile_togglemenu").remove();
			}	
		}
		jQuery(document).ready(function(){footerToggleMenu();});
		jQuery(window).resize(function(){footerToggleMenu();}); 

	
		function sidebarToggle(){
			if (jQuery(window).width() < 979)
			{
				jQuery(".sidebar .block .mobile_togglemenu").remove();
				jQuery(".sidebar .block .block-title").append( "<a class='mobile_togglemenu'>&nbsp;</a>" );
				jQuery(".sidebar .block .block-title").addClass('toggle');
				jQuery(".sidebar .block .mobile_togglemenu").click(function(){
					jQuery(this).parent().toggleClass('active').parent().find('.block-content').slideToggle('slow');
				});
			}else{
				jQuery(".sidebar .block .block-title").parent().find('.block-content').removeAttr('style');
				jQuery(".sidebar .block .block-title").removeClass('active');
				jQuery(".sidebar .block .block-title").removeClass('toggle');
				jQuery(".sidebar .block .mobile_togglemenu").remove();
			}	
		}
		jQuery(document).ready(function(){sidebarToggle();});
		jQuery(document).load(function(){sidebarToggle();});
		jQuery(window).resize(function(){sidebarToggle();}); 
		
	});
});